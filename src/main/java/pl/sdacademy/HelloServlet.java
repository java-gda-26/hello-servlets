package pl.sdacademy;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String firstName = request.getParameter("firstName");
        if (firstName == null || firstName.isEmpty()) {
            firstName = "Servlets";
        }

        request.setAttribute("firstName", firstName);
        request.getRequestDispatcher("index.jsp")
                .forward(request, response);
    }
}
